import {
  createAsyncThunk,
  createSelector,
  createSlice,
} from "@reduxjs/toolkit";
import { fetchCryptoHistory, fetchCryptos } from "./cryptoCurrencyAPI";
import { RootState } from "../../app/store";

export interface ICrypto {
  changePercent24Hr: string;
  explorer: string;
  id: string;
  marketCapUsd: string;
  maxSupply: string;
  name: string;
  priceUsd: string;
  rank: string;
  supply: string;
  symbol: string;
  volumeUsd24Hr: string;
  isFavorite?: boolean;
}

export interface ICryptoCurrencyState {
  value: number;
  status: "idle" | "loading" | "failed";
  list: ICrypto[];
  listCryptoIds: string[];
  lastIncreasedCryptos: string[];
  lastDecreasedCryptos: string[];
  lastUpdate: number;
  detailHistory: IChartPoint[];
  selectedState: string;
}

export interface IChartPoint {
  value: number;
  time: number;
}

const initialState: ICryptoCurrencyState = {
  value: 0,
  status: "idle",
  list: [],
  listCryptoIds: [],
  lastIncreasedCryptos: [],
  lastDecreasedCryptos: [],
  lastUpdate: Date.now(),
  detailHistory: [],
  selectedState: "",
};

export const loadCryptos = createAsyncThunk("crypto/connectWS", async () => {
  const response = await fetchCryptos();
  // The value we return becomes the `fulfilled` action payload
  return response.data;
});
export const loadCryptoHistory = createAsyncThunk(
  "crypto/fetchHistory",
  async (id: string) => {
    const response = await fetchCryptoHistory(id);
    // The value we return becomes the `fulfilled` action payload
    return { data: response.data, id };
  }
);

export const cryptoSlice = createSlice({
  name: "crypto",
  initialState,
  reducers: {
    updatePrices: (state, action) => {
      if (state.list.length === 0) return state;
      const updatedCryptos = action.payload;
      const arrayOfKeys = Object.keys(updatedCryptos);
      let atLeastOneUpdate = 0;
      let lastDecreasedCryptos: string[] = [];
      let lastIncreasedCryptos: string[] = [];
      arrayOfKeys.forEach((currencyName) => {
        const idx = state.list.findIndex((c) => {
          if (c === undefined) return -1;
          return c.id === currencyName;
        });
        if (idx > -1) {
          const currentPrice: number = Number(state.list[idx].priceUsd);
          const updatedPrice: number = Number(updatedCryptos[currencyName]);
          const diff = currentPrice - updatedPrice;
          if (Math.abs(diff) > 0.01 && diff > 0) {
            // it decreased
            state.list[idx].priceUsd = updatedCryptos[currencyName];
            lastDecreasedCryptos.push(currencyName);
            atLeastOneUpdate += 1;
          } else if (Math.abs(diff) > 0.01 && diff < 0) {
            // it decreased
            state.list[idx].priceUsd = updatedCryptos[currencyName];
            lastIncreasedCryptos.push(currencyName);
            atLeastOneUpdate += 1;
          }
          if (currencyName === state.selectedState)
            state.detailHistory.push({
              value: updatedCryptos[currencyName],
              time: Date.now(),
            });
        }
        if (atLeastOneUpdate > 0) {
          state.lastUpdate = Date.now();
          state.lastIncreasedCryptos = lastIncreasedCryptos;
          state.lastDecreasedCryptos = lastDecreasedCryptos;
        }
        // if (currencyName && currencyName.length > 0) {
        // }
      });
    },
    clearDetailHistory: (state) => {
      state.detailHistory = [];
      state.selectedState = "";
    },
    toggleFavorite: (state, action) => {
      let storedFavList: string[] = [];
      state.list = state.list.map((c) => {
        if (c.id === action.payload) c.isFavorite = !c.isFavorite;
        if (c.isFavorite) storedFavList.push(c.id);
        return c;
      });
      localStorage.setItem("favList", storedFavList.toString());
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadCryptos.pending, (state) => {
        state.status = "loading";
      })
      .addCase(loadCryptos.fulfilled, (state, action) => {
        const storedFavListItem = localStorage.getItem("favList");
        let storedFavList: string[] = [];
        let stateList: ICrypto[] = [];
        if (storedFavListItem) storedFavList = storedFavListItem.split(",");
        action.payload.forEach((i) => {
          if (storedFavList.includes(i.id)) i.isFavorite = true;
          stateList.push(i);
        });
        state.status = "idle";
        state.list = stateList;
        state.listCryptoIds = action.payload.reduce((acc: string[], curr) => {
          return acc.concat(curr.id);
        }, []);
      })
      .addCase(loadCryptoHistory.fulfilled, (state, action) => {
        if (action.payload.data) {
          action.payload.data.forEach((r: any) => {
            state.detailHistory.push({
              value: Number(r.priceUsd),
              time: r.time,
            });
          });
          state.selectedState = action.payload.id;
        }
      });
  },
});
export const { updatePrices, toggleFavorite } = cryptoSlice.actions;

export const selectCryptos = (state: RootState) => state.crypto.list;
export const selectCryptoIds = (state: RootState) => state.crypto.listCryptoIds;
export const selectIncreasedCryptoIds = (state: RootState) =>
  state.crypto.lastIncreasedCryptos;
export const selectDecreasedCryptoIds = (state: RootState) =>
  state.crypto.lastDecreasedCryptos;
export const selectLastUpdate = (state: RootState) => state.crypto.lastUpdate;
export const selectStatus = (state: RootState) => state.crypto.status;
export const selectHistory = (state: RootState) => state.crypto.detailHistory;

export const getFavoriteCryptos = createSelector(
  (state: RootState) => state.crypto.list,
  (list: ICrypto[]) => list.filter((c) => c.isFavorite)
);

export default cryptoSlice.reducer;
