import React from "react";
import { Fragment } from "react";
import { ICrypto } from "./cryptoCurrencySlice";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core";
import { TableBodyComponent } from "./commons/tableBodyComponent";

interface IProps {
  cryptos: ICrypto[];
  lastIncreased?: string[];
  lastDecreased?: string[];
}
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function CryptosList(props: IProps) {
  const classes = useStyles();
  const { cryptos, lastDecreased, lastIncreased } = props;
  return (
    <Fragment>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Price</TableCell>
              <TableCell align="right">(24H)</TableCell>
            </TableRow>
          </TableHead>
          {cryptos.length > 0 && (
            <TableBodyComponent
              cryptos={cryptos}
              lastIncreased={lastIncreased ? lastIncreased : []}
              lastDecreased={lastDecreased ? lastDecreased : []}
            />
          )}
        </Table>
      </TableContainer>
    </Fragment>
  );
}

export default CryptosList;
