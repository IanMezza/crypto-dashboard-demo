import React from "react";
import { useAppSelector } from "../../../app/hooks";
import {
  ICrypto,
  selectIncreasedCryptoIds,
  selectDecreasedCryptoIds,
  getFavoriteCryptos,
} from "../cryptoCurrencySlice";
import CryptosList from "../cryptosList";
import { connect } from "react-redux";

function FavoriteCryptos(props: { cryptos: ICrypto[] }) {
  const lastIncreasedCryptos = useAppSelector(selectIncreasedCryptoIds);
  const lastDecreasedCryptos = useAppSelector(selectDecreasedCryptoIds);

  return (
    <div>
      <h1>Favorite Crypto Currency List</h1>
      <CryptosList
        cryptos={props.cryptos}
        lastIncreased={lastIncreasedCryptos}
        lastDecreased={lastDecreasedCryptos}
      />
    </div>
  );
}

const mapStateToProps = (state: any) => ({
  cryptos: getFavoriteCryptos(state),
});

export default connect(mapStateToProps)(FavoriteCryptos);
