import * as React from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import CryptosList from "../cryptosList";
import {
  createChart,
  LineData,
  UTCTimestamp,
  WhitespaceData,
} from "lightweight-charts";
import { Fragment, useEffect } from "react";
import {
  loadCryptoHistory,
  selectHistory,
  toggleFavorite,
} from "../cryptoCurrencySlice";
import { Checkbox } from "@material-ui/core";

// @ts-ignore

interface IProps {
  id: string;
}

export const CryptoDetail = (props: RouteComponentProps<IProps>) => {
  const { id } = props.match.params;
  const dispatch = useAppDispatch();
  const currentCrypto = useAppSelector((state) =>
    state.crypto.list.find((c) => c.id === id)
  );
  const history = useAppSelector(selectHistory);
  useEffect(() => {
    dispatch(loadCryptoHistory(id));
  }, []);

  useEffect(() => {
    /**
     * I think there should be a better way to update the chart, x axis should be responsive
     * relative to new updates. It doesn't make much sense to have displayed 6 days ago and then
     * have an update on real time.
     */
    const chartContainer = document.getElementById("chartWrapper");
    if (chartContainer) {
      chartContainer.innerHTML = "";
      // todo: chart should be rendered using a ref instead of an id
      const chart = createChart("chartWrapper", {
        height: 300,
        localization: {
          dateFormat: "yyyy/MM/dd",
        },
      });
      const updatedData: (LineData | WhitespaceData)[] = [];
      /**
       * For some reason values coming from redux store must be cloned
       * before they're sent to the addLineSeries function
       *
       */
      history.forEach((i) => {
        updatedData.push({
          value: i.value,
          time: (i.time / 1000) as UTCTimestamp,
        });
      });
      const lineSeries = chart.addLineSeries();
      lineSeries.setData(updatedData);
    }
  }, [history]);

  return (
    <div>
      <Link to="/cryptos">Back to List</Link>
      {currentCrypto && (
        <Fragment>
          <h1>{currentCrypto.name}</h1>
          <p>
            <Checkbox
              checked={currentCrypto.isFavorite}
              onClick={() => dispatch(toggleFavorite(currentCrypto.id))}
            />{" "}
            Add to Favorites
          </p>
          <CryptosList cryptos={currentCrypto ? [currentCrypto] : []} />
          <div id="chartWrapper" style={{ marginTop: "2rem" }}></div>
        </Fragment>
      )}
    </div>
  );
};
