// @flow
import * as React from "react";
import TableCell from "@material-ui/core/TableCell";
import { ICrypto } from "../cryptoCurrencySlice";
import { Link } from "react-router-dom";

interface IProps {
  row: ICrypto;
  lastIncreased?: string[];
  lastDecreased?: string[];
}

export const DataGridRow = ({ row, lastDecreased, lastIncreased }: IProps) => {
  let cellStyle = "unchanged";
  if (lastDecreased && lastDecreased.includes(row.id)) {
    cellStyle = "decreased";
  } else if (lastIncreased && lastIncreased.includes(row.id)) {
    cellStyle = "increased";
  }
  return (
    <>
      <TableCell className={cellStyle} component="th" scope="row">
        <Link to={`/cryptos/${row.id}`}>{row.name}</Link>
      </TableCell>
      <TableCell className={cellStyle} align="right">
        ${Number(row.priceUsd).toFixed(2)}
      </TableCell>
      <TableCell className={cellStyle} align="right">
        {Number(row.changePercent24Hr).toFixed(2)}%
      </TableCell>
    </>
  );
};
