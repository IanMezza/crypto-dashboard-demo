// @flow
import * as React from "react";
import TableRow from "@material-ui/core/TableRow";
import { TableBody } from "@material-ui/core";
import { ICrypto } from "../cryptoCurrencySlice";
import { DataGridRow } from "./DataGridRow";

interface IProps {
  cryptos: ICrypto[];
  lastIncreased?: string[];
  lastDecreased?: string[];
}

export function TableBodyComponent({
  cryptos,
  lastIncreased,
  lastDecreased,
}: IProps) {
  return (
    <TableBody>
      {cryptos.map((row) => {
        if (row) {
          return (
            <TableRow key={row.id}>
              <DataGridRow
                row={row}
                lastIncreased={lastIncreased}
                lastDecreased={lastDecreased}
              />
            </TableRow>
          );
        } else return null;
      })}
    </TableBody>
  );
}
