import React, { useEffect } from "react";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import {
  ICrypto,
  loadCryptos,
  selectCryptos,
  selectLastUpdate,
  selectIncreasedCryptoIds,
  selectDecreasedCryptoIds,
} from "./cryptoCurrencySlice";
import CryptosList from "./cryptosList";

export function CryptoCurrency() {
  const cryptos: ICrypto[] = useAppSelector(selectCryptos);
  const lastUpdate: number = useAppSelector(selectLastUpdate);
  const lastIncreasedCryptos = useAppSelector(selectIncreasedCryptoIds);
  const lastDecreasedCryptos = useAppSelector(selectDecreasedCryptoIds);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (cryptos.length === 0) {
      dispatch(loadCryptos());
    }
  }, []);

  return (
    <div>
      <h1>Crypto Currency List</h1>
      <CryptosList
        cryptos={cryptos}
        lastIncreased={lastIncreasedCryptos}
        lastDecreased={lastDecreasedCryptos}
      />
    </div>
  );
}
