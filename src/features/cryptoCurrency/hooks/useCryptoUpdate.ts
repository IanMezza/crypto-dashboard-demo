import { updatePrices } from "../cryptoCurrencySlice";
import { useAppDispatch } from "../../../app/hooks";

export default function useCryptoUpdate(cryptos: string[]) {
  // export default function useCryptoUpdate() {
  const dispatch = useAppDispatch();
  // Create WebSocket connection.
  if (cryptos.length > 0) {
    const socket = new WebSocket(
      `wss://ws.coincap.io/prices?assets=${encodeURI(cryptos.toString())}`
    );

    // Connection opened
    socket.addEventListener("open", function (event) {
      socket.send("Hello Server!");
    });

    // Listen for messages
    socket.addEventListener("message", function (event) {
      const filteredUpdates = getNewPrices(event.data, cryptos);
      if (Object.keys(filteredUpdates).length > 0)
        dispatch(updatePrices(filteredUpdates));
    });
  }
}

/**
 * Get an update dictionary for currencies present in original table
 * @param wsData
 * @param cryptos
 */
const getNewPrices = (wsData: any, cryptos: string[]) => {
  const updatedCryptos = JSON.parse(wsData);
  const arrayOfKeys = Object.keys(updatedCryptos);
  let updatedPricesDict = {};
  arrayOfKeys.forEach((currencyName) => {
    if (cryptos.includes(currencyName)) {
      Object.defineProperty(updatedPricesDict, currencyName, {
        value: updatedCryptos[currencyName],
        enumerable: true,
        writable: false,
      });
    }
  });
  return updatedPricesDict;
};
