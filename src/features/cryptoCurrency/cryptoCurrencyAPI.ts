import axios from "axios";
import { IChartPoint, ICrypto } from "./cryptoCurrencySlice";

export function fetchCryptos() {
  return new Promise<{ data: ICrypto[] }>((resolve) =>
    axios
      .get("https://api.coincap.io/v2/assets?limit=20")
      .then(function (response) {
        // handle success
        resolve(response.data);
      })
      .catch(function (error) {
        // handle error
        resolve(error);
      })
  );
}

export function fetchCryptoHistory(id: string) {
  return new Promise<{ data: ICrypto[] }>((resolve) =>
    axios
      .get(`https://api.coincap.io/v2/assets/${id}/history?interval=m30`)
      .then(function (response) {
        // handle success
        resolve(response.data);
      })
      .catch(function (error) {
        // handle error
        resolve(error);
      })
  );
}
