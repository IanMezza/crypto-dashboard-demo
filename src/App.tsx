import React from "react";
import "./App.css";
import { CryptoCurrency } from "./features/cryptoCurrency/CryptoCurrency";
import { Breadcrumbs, Container, Link, Typography } from "@material-ui/core";
import useCryptoUpdate from "./features/cryptoCurrency/hooks/useCryptoUpdate";
import { selectCryptoIds } from "./features/cryptoCurrency/cryptoCurrencySlice";
import { useAppSelector } from "./app/hooks";
import { Route, Switch, Redirect } from "react-router-dom";
import { Link as NavLink } from "react-router-dom";
import { CryptoDetail } from "./features/cryptoCurrency/components/cryptoDetail";
import FavoriteCryptos from "./features/cryptoCurrency/components/favoriteCryptos";

function App() {
  const cryptos: string[] = useAppSelector(selectCryptoIds);
  useCryptoUpdate(cryptos);
  return (
    <div className="App">
      <Container maxWidth={"md"}>
        <Breadcrumbs aria-label="breadcrumb">
          <NavLink to="/">Home</NavLink>
          <NavLink to="/cryptos/favorites">Favorites</NavLink>
        </Breadcrumbs>
        <Switch>
          <Route path="/cryptos/favorites" component={FavoriteCryptos} />
          <Route path="/cryptos/:id" component={CryptoDetail} />
          <Route path="/cryptos" component={CryptoCurrency} />
          <Redirect from="/" to="/cryptos" />
        </Switch>
      </Container>
    </div>
  );
}

export default App;
