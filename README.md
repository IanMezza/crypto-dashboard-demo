# Crypto Dash

Demo de una aplicación web construida con React.

Básicamente consume información de una [API de criptomonedas](https://docs.coincap.io/#37dcec0b-1f7b-4d98-b152-0217a6798058) 
para su funcionamiento.

Al arrancar la aplicación, ésta consulta una lista de 20 criptomonedas y utiliza los Ids de éstas para abrir una conexión 
con Websocket para actulizar los precios de las criptomonedas en tiempo real. Los cambios se hacen notar en la interfaz 
a través de animaciones de cambio de color, denotando incrementos (color verde) y decrementos (color rojo).

Al hacer click sobre el nombre de cualquiera de las monedas en la lista, se dirije al usuario a una nueva pantalla con el 
detalle de la moneda seleccionada. Aquí se muestra además de la información básica de la moneda, una gráfica que actualiza
en tiempo real el comportamiento de cambio del precio de la misma. Dentro de ésta misma pantalla el usuario puede elegir
seleccionar (o no) si quiere tener la moneda en la lista de favoritos.

Por último, la aplicación cuenta con una tercera pantalla que muestra la lista de las monedas que han sido marcadas como 
favoritas. Ésta lista es persistente, al utilizar *localStorage* los cambios quedan almacenados en el navegador del usuario.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.
